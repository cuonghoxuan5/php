<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>hiển thị thời gian</title>
</head>
<body>
<?php
 date_default_timezone_set('Asia/Tokyo'); 
 ?>
<h1> Bây giờ là: <?php echo date(' h:i:s Y-M-D-d'); ?> </h1>
</body>
</html>